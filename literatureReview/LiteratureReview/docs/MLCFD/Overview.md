(part_MLAndFluids)=
# Overview

Notes based on this excellent review article: {cite}`Brunton2020`

## Machine Learning as Optimization Technique
* Machine learning (ML) is about building models from data using optimization.
* It can be understood in terms of linear algebra, optimization and statistics.

* In focusing on the foundation of ML in these fields, the basis is provided for developing interpretable and generalizable ML techniques that can be trusted.
* This approach prevents ML from becoming a black box technique and allows its applications even for situations where it has not been applied before.

* Today it is mostly applied in the field of computer vision (image recognition, image reconstruction). 
* Historically ML originated from the drag reduction analysis of a flat plate [Reichenberg/Scheifel; 1969s/70s].
* Hence a close connection between ML and fluid dynamics can be stated *ab initio*.


## Fluid Dynamics as Optimization Problem
* Fluid dynamics tasks are in general
    * multiscale and hence higher-dimensional
    * nonlinear and
    * non-convex
* They constitutes  a vast amount of data (generated by experiments or simulations).
* Fluid dynamics tasks can be (naturally) rewritten as optimization problems.
* Besides modeling itself the following tasks in the field of fluid dynamics can be regarded as optimization tasks as well:
    * closure (reducing complexity)
    * reduction (reduced order modeling)
    * control (controlling fluid dynamical systems)
    * sensing (determine optimal sensor locations)
* Since machine learning is in principal building models from data using optimization, machine learning is highly suited for the optimization tasks arising in the field of fluid dynamics (ML as natural fit for solving fluid dynamics tasks).


## Application of Machine Learning to Fluid Dynamics
* Fluid dynamics generate a lot of data; either from experiments, theory or simulations.
* ML uses these data to develop models which can be applied to tackle specific fluid dynamic tasks, such as
    * modeling
    * reduction
    * control
    * optimization









